import os
import csv
import sqlite3
import requests

from typing import List, Optional
from pathlib import Path

from fastapi import FastAPI, Depends, HTTPException, Query
from sqlmodel import Field, Session, SQLModel, create_engine, select

from app.models import CatRead, Cat, CatCreate, CatWithHobbies
from app.models import HobbyRead, Hobby, HobbyCreate
from app.db.database import create_db_and_tables, engine


CATS_API_BASE_URL = "https://cdn2.thecatapi.com/images"
FTP_FILE = os.path.join('/home','maobe', os.environ['FTP_FILEPATH'])


def read_csv_file():
    with open(FTP_FILE, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=';')
        for row in csv_reader:
            yield row


def get_session():
    with Session(engine) as session:
        yield session


app = FastAPI()


@app.on_event("startup")
def on_startup():
    create_db_and_tables()


@app.post("/cats/", response_model=CatRead)
def create_cat(*, session: Session = Depends(get_session)):
    for cat in read_csv_file():
        cat_id = cat['id']
        name = cat['name']
        age = cat['age']
        color = cat['color']
        hobby = cat['hobbies']
        url = f"{CATS_API_BASE_URL}/{cat_id}.jpg"
        response = requests.get(url)
        picture = url if response.status_code == 200 else None
        cat = CatCreate(
                name=name,
                id=cat_id,
                age=age,
                color=color,
                picture=picture
        )
        hobby = HobbyCreate(name=hobby, cat_id=cat.id)
        try:
            db_cat = Cat.from_orm(cat)
            session.add(db_cat)
            session.commit()
            session.refresh(db_cat)

            db_hobby = Hobby.from_orm(hobby)
            session.add(db_hobby)
            session.commit()
            session.refresh(db_hobby)
        except:
            raise HTTPException(status_code=404, detail="Cat already registered")
    return db_cat


@app.get("/cats/", response_model=List[CatWithHobbies])
def read_cats(
    *,
    session: Session = Depends(get_session),
    offset: int = 0,
    limit: int = Query(default=100, lte=100),
):
    cats = session.exec(select(Cat).offset(offset).limit(limit)).all()
    return cats


@app.delete("/cats/{cat_id}")
def delete_cat(*, session: Session = Depends(get_session), cat_id: str):
    cat = session.get(Cat, cat_id)
    if not cat:
        raise HTTPException(status_code=404, detail="Cat not found")
    session.delete(cat)
    session.commit()
    return {"ok": True}
