from .cats import Cat, CatRead, CatCreate, CatWithHobbies
from .hobbies import Hobby, HobbyRead, HobbyBase, HobbyCreate
