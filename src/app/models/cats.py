from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel
from .hobbies import HobbyRead


class CatBase(SQLModel):
    name: str = Field(index=True)
    age: Optional[int] = None
    color: Optional[str] = None
    picture: Optional[str] = None


class Cat(CatBase, table=True):
    id: Optional[str] = Field(default=None, primary_key=True)

    hobbies: List['Hobby'] = Relationship(back_populates='cat')


class CatCreate(CatBase):
    id: Optional[str] = None


class CatUpdate(SQLModel):
    name: Optional[str] = None
    age: Optional[int] = None
    color: Optional[str] = None
    picture: Optional[str] = None


class CatRead(CatBase):
    id: str


class CatWithHobbies(CatRead):
    hobbies: List[HobbyRead] = None
