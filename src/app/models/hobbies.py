from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel


class HobbyBase(SQLModel):
    name: str = Field(index=True)

    cat_id: Optional[str] = Field(default=None, foreign_key="cat.id")


class Hobby(HobbyBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    cat: Optional['Cat'] = Relationship(back_populates="hobbies")



class HobbyCreate(HobbyBase):
    id: Optional[int] = None


class HobbyUpdate(SQLModel):
    name: Optional[str] = None


class HobbyRead(HobbyBase):
    id: int
