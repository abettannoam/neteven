# Neteven challenge

## Installation
The only requirement is to install docker-compose using your package manager (for instance)
```bash
pip install docker-compose
```

## Usage
```bash
docker-compose up --build
```
will build the project and set tables database.

You can then interact with auto-generated api by Swagger. (localhost:8000/docs)


## Reference
[Challenge](https://gitlab.com/neteven/python_interview/)
